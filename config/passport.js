const passport = require("passport");
const { facebook } = require("../config");
const config = require("../config");
const LocalStrategy = require("passport-local").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const GoogleStrategy = require("passport-google-oauth20").Strategy;

var Usuario = require("../models/usuario");

passport.serializeUser(function (usuario, done) {
  done(null, usuario.id);
});

passport.deserializeUser(function (id, done) {
  Usuario.findById(id, function (err, usuario) {
    done(err, usuario);
  });
});

passport.use(
  new LocalStrategy(function (email, password, done) {
    console.log("host SFace: " + process.env.HOST);
    Usuario.findOne({ email: email }, function (err, usuario) {
      if (err) return done(err);
      if (!usuario)
        return done(null, false, { message: "El Email no existe." });

      if (!usuario.validPassword(password))
        return done(null, false, { message: "El Password es incorrecto." });

      return done(null, usuario);
    });
  })
);

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
      callbackURL: process.env.FACEBOOK_CALLBACK_URL,
      enableProof: true,
      profileFields: ["id", "displayName", "email", "photos"],
    },
    function (accessToken, refreshToken, profile, done) {
      // asynchronous
      console.log("host SFace: " + process.env.HOST);
      console.log(JSON.stringify(profile));
      process.nextTick(function () {
        Usuario.findOne({ provider_id: profile.id }, function (err, usuario) {
          if (err) return done(err);
          if (!err && usuario != null) return done(null, usuario);

          console.log(JSON.stringify(profile));
          var usuario = new Usuario({
            provider_id: profile.id,
            provider: profile.provider,
            password: accessToken,
            nombre: profile.displayName,
            email: profile.emails[0].value,
            photo: profile.photos[0].value,
            verificado: true,
          });

          usuario.save(function (err) {
            if (err) throw err;
            return done(null, usuario);
          });
        });
      });
    }
  )
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.GOOGLE_CALLBACK_URL,
    },
    function (accessToken, refreshToken, profile, cb) {
      Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
        return cb(err, user);
      });
    }
  )
);

module.exports = passport;
