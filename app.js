require("newrelic");
require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var mongoose = require("mongoose");
var passport = require("./config/passport");
var session = require("express-session");
var jwt = require("jsonwebtoken");
var MongoDBStore = require("connect-mongodb-session")(session);

var indexRouter = require("./routes/index");
var usuariosRouter = require("./routes/usuarios");
var tokenRouter = require("./routes/token");

var bicicletasRouter = require("./routes/bicicletas");
var bicicletasApiRouter = require("./routes/api/bicicletas");
var usuariosApiRouter = require("./routes/api/usuarios");
var authApiRouter = require("./routes/api/auth");
var loginRouter = require("./routes/login");

const { Console, assert } = require("console");
var app = express();

/**Configuracion Session Storage */

var store;

if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "session",
    expires: 1000 * 60 * 60 * 24 * 30,
    connectionOptions: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      serverSelectionTimeoutMS: 10000,
    },
  });
  store.on("error", function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
}

/**Cifrar los token de json */
app.set("secretKey", "jwt_pwd_!!223344");

app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: "red_bicis_!!!**!!!***!**!",
  })
);

//Set up default mongoose connection
// var mongoDB = "mongodb://localhost/red_bicicleta";
//var mongoAtlas = "mongodb+srv://admin:NDxvycVOO5yAjm6Q@redbicicletasa.eqc9s.mongodb.net/test";

var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind(console, "MongoDB connection error:"));
db.once("open", function () {
  console.log("conectado a mongo");
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "src")));

// Using the flash middleware provided by connect-flash to store messages in session
// and displaying in templates
var flash = require("connect-flash");
app.use(flash());

app.use("/", indexRouter);
app.use("/login", loginRouter);
app.use("/bicicletas", loggedIn, bicicletasRouter);
app.use("/usuarios", loggedIn, usuariosRouter);
app.use("/token", tokenRouter);

app.use("/api/bicicletas", validarUsuario, bicicletasApiRouter);
app.use("/api/usuarios", usuariosApiRouter);
app.use("/api/auth", authApiRouter);

app.use("/google0fb2388b257c19da", function (req, res) {
  res.sendFile(path.join(__dirname, "src", "google0fb2388b257c19da.html"));
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    console.log("Usuario sin logearse");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  let token = req.headers("x-access-token");
  if (!token) {
    return res.json({ status: "error", message: "No envió un token" });
  }

  jwt.verify(token, req.app.get("secretKey"), function (err, decode) {
    if (err) {
      res.json({
        status: "error",
        message: err.message,
        data: null,
      });
    } else {
      req.body.userId = decode.id;
      console.log("jwt verify: " + decode);
      next();
    }
  });
}

module.exports = app;
