const Bicicleta = require("../../models/bicicleta");
const request = require("request");
const server = require("../../bin/www");
const mongoose = require("mongoose");

var base_url = "http://localhost:3000/api/bicicletas";
describe("BICICLETA API", () => {
  beforeEach(function (done) {
    mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error."));
    db.once("open", function () {
      console.log("conectado a la base de datos");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("GET bicicletas/", () => {
    it("status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });
  describe("POST bicicletas/create", () => {
    it("status 200", (done) => {
      const headers = { "content-type": "application/json" };
      const aBici =
        '{"code": 10,"color": "verde","modelo": "Electrica","lat": 10.794131,"lng": -74.917513}';
      request.post(
        {
          headers: headers,
          url: base_url+"/create",
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          let bici = JSON.parse(body).bicicleta;
          console.log(bici);

          expect(bici.color).toBe("verde");
          expect(bici.ubicacion[0]).toBe(10.794131);
          expect(bici.ubicacion[1]).toBe(-74.917513);

          done();
        }
      );
    });
  });
  describe("post bicicleta /delete", () => {
    it("status 204", (done) => {
      var headers = { "content-type": "application/json" };
      var aBici = '{"code":1}';

      request.delete(
        {
          headers: headers,
          url: base_url + "/delete",
          body: aBici,
        },
        function (error, eliminar) {
          done();
        }
      );
    });
  });
});

/**Metodos de prueba - sin persistencia */
/*
describe("Bicicleta Api", () => {
  describe("Bicicleta Get", () => {
    it("Status 200", () => {
      expect(Bicicleta.allBicis.length).toBe(0);

      var a = new Bicicleta(1, "Negro", "Ruta", [10.796254, -74.914412]);
      Bicicleta.add(a);

      request.get("http://localhost:3000/api/bicicletas", function (
        error,
        response,
        body
      ) {
        expect(response.statusCode).toBe(200);
      });
    });
  });

  describe("Post Bicicleta/create ", () => {
    it("Status 200", (done) => {
      var headers = { "content-type": "application/json" };
      var abici =
        '{"id":10,"color":"Azul","modelo":"Electrica","lat":10.794131,"lng": -74.917513}';
      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/create",
          body: abici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          expect(Bicicleta.findById(10).color).toBe("Azul");
          done();
        }
      );
    });
  });
  describe("Post Bicicleta/create ", () => {
    it("Status 200", (done) => {
      var headers = { "content-type": "application/json" };
      var abici =
        '{"id":10,"color":"Azul","modelo":"Electrica","lat":10.794131,"lng": -74.917513}';
      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/create",
          body: abici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          expect(Bicicleta.findById(10).color).toBe("Azul");
          done();
        }
      );
    });
  });
});
*/
