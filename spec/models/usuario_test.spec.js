const mongoose = require("mongoose");
const Usuario = require("../../models/usuario");

describe("Testing Usuario Mongoose", function () {
  beforeEach(function (done) {
    var mongoDB = "mongodb://127.0.0.1/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });
    //Get the default connection
    var db = mongoose.connection;
    //Bind connection to error event (to get notification of connection errors)
    db.on("error", console.error.bind(console, "MongoDB connection error:"));
    db.once("open", function () {
      console.log("se connecto a la base de datos testdb");
      done();
    });
  });

  afterEach(function (done) {
    Usuario.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("Usuario.createInstance", () => {
    it("crea una instancia de Usuario", () => {
      var u = Usuario.createInstance(
        "Andrew Estrada Ariza",
        "andrewestradaariza@gmail.com",
        "123"
      );
      expect(u.nombre).toBe("Andrew Estrada Ariza");
      expect(u.email).toBe("andrewestradaariza@gmail.com");
      expect(u.password).toBe("123");
    });
  });

  /**fin */
});
