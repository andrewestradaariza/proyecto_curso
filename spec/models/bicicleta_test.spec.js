const mongoose = require("mongoose");
const Bicicleta = require("../../models/bicicleta");

describe("Testing Bicicletas Mongoose", function () {
  beforeEach(function (done) {
    var mongoDB = "mongodb://127.0.0.1/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });
    //Get the default connection
    var db = mongoose.connection;
    //Bind connection to error event (to get notification of connection errors)
    db.on("error", console.error.bind(console, "MongoDB connection error:"));
    db.once("open", function () {
      console.log("se connecto a la base de datos testdb");
      done();
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("crea una instancia de Bicicleta", () => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [
        10.794131,
        -74.917513,
      ]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toEqual(10.794131);
      expect(bici.ubicacion[1]).toEqual(-74.917513);
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("comienza vacia", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("agrega solo una Bici", (done) => {
      var aBici = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
      Bicicleta.add(aBici, function (err, newBici) {
        if (err) console.log(err);

        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("debe devolver la bici con code: 1", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana",
        });

        Bicicleta.add(aBici, (err, newBici) => {
          if (err) console.log(err);

          var aBici2 = new Bicicleta({
            code: 2,
            color: "azul",
            modelo: "montaña",
          });

          Bicicleta.add(aBici2, (err, newBici2) => {
            if (err) console.log(err);
            Bicicleta.findByCode(1, (error, targetBici) => {
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });

  describe("Bicicleta.removeByCode", () => {
    it("debe eliminar la bici con code 1", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);

        var aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana",
        });

        Bicicleta.add(aBici, (err, newBici) => {
          if (err) console.log(err);

          Bicicleta.removeByCode(1, (err) => {
            if (err) console.log(err);

            expect(bicis.length).toBe(0);
            done();
          });
        });
      });
    });
  });
  /**fin */
});

/**Antes de cada prueba 
beforeAll(() => {
  console.log("testeando...");
});

describe("Bicicleta.allBicis", () => {
  it("comienza vacia", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe("Bicicleta.add", () => {
  it("agregamos una", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1, "Rojo", "Urbana", [10.794131, -74.917513]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe("Bicicleta.findById", () => {
  it("debe devolver la bicicleta con id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var aBici = new Bicicleta(1, "Verde", "Urbana");
    var aBici2 = new Bicicleta(2, "Rojo", "Montaña");
    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);
  });
});

describe("Bicicleta.removeById", () => {
  it("debe eliminar el id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var aBici = new Bicicleta(1, "Verde", "Urbana");
    Bicicleta.add(aBici);

    expect(Bicicleta.allBicis.length).toBe(1);
    Bicicleta.removeById(1);
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});
*/
