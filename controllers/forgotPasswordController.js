const passport = require("../config/passport");
const Usuario = require("../models/usuario");
const Token = require("../models/token");

module.exports = {
  forgot_password_get: function (req, res, next) {
    res.render("login/forgotPassword", { title: "Reseteo de password" });
  },
  forgot_password_post: function (req, res, next) {
    Usuario.findOne({ email: req.body.email }, function (err, usuario) {
      if (!usuario) {
        return (
          res.render("login/forgotPassword"),
          {
            title: "Reseteo de password",
            info: { message: "No existe el email para un usuario existente." },
          }
        );
      }
      usuario.resetPassword(function (err) {
        if (err) return next(err);
        console.log("login/forgotPasswordMessage");
      });
      res.render("login/forgotPasswordMessage");
    });
  },
  reset_password_get: function (req, res, next) {
    Token.findOne({ token: req.params.token }, function (err, token) {
      if (!token)
        return res.status(400).send({
          type: "not-verified",
          message:
            "No existe usuario asociado al token, verifique que su token no haya expirado.",
        });
      Usuario.findById(token._usuarioId, function (err, usuario) {
        if (!usuario)
          return res
            .status(400)
            .send({ message: "No existe un usuario asociado al token." });
        res.render("login/resetPassword", { errors: {}, usuario: usuario });
      });
    });
  },
  reset_password_post: function (req, res, next) {
    if (req.body.password != req.body.confirm_password) {
      res.render("login/resetPassword", {
        errors: {
          confirm_password: {
            message: "No coincide con el password ingresado",
          },
        },
        usuario: new Usuario({ email: req.body.email }),
      });
      return;
    }
    Usuario.findOne({ email: req.body.email }, function (err, usuario) {
      usuario.password = req.body.password;
      usuario.save(function (err) {
        if (err) {
          res.render("login/resetPassword", {
            errors: err.errors,
            usuario: new Usuario({ email: req.body.email }),
          });
        } else {
          res.redirect("/login");
        }
      });
    });
  },
};
