/**Se instancia el modelo */
const Bicicleta = require("../models/bicicleta");

module.exports = {
  list: function (req, res, next) {
    Bicicleta.find({}, (err, bicis) => {
      res.render("bicicletas/index", {
        bicis: bicis,
        title: "Bicicleta | Listado",
      });
    });
  },
  create_get: function (req, res, next) {
    res.render("bicicletas/create", {
      errors: {},
      bicicleta: new Bicicleta(),
      title: "Bicicletas | Crear",
    });
  },
  create_post: function (req, res, next) {
    Bicicleta.create(
      {
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng],
      },
      (err, nuevaBicicleta) => {
        if (err) {
          console.error(err.message);
          res.render("bicicletas/create", {
            errors: err.errors,
            bicicleta: new Bicicleta({
              code: req.body.code,
              color: req.body.color,
              modelo: req.body.modelo,
              ubicacion: [req.body.lat, req.body.lng],
            }),
          });
        } else {
          res.redirect("/bicicletas");
        }
      }
    );
  },
  update_get: function (req, res, next) {
    Bicicleta.findById(req.params.id, (err, bici) => {
      res.render("bicicletas/update", { errors: {}, bici: bici });
    });
  },
  update_post: function (req, res, next) {
    let update_values = {
      code: req.body.code,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.lat, req.body.lng],
    };
    Bicicleta.findByIdAndUpdate(
      req.params.id,
      update_values,
      (err, bicicleta) => {
        if (err) {
          console.error(err.message);
          res.render("bicicletas/update", {
            errors: err.errors,
            bici: new Bicicleta({
              code: req.body.code,
              color: req.body.color,
              modelo: req.body.modelo,
              ubicacion: [req.body.lat, req.body.lng],
            }),
          });
        } else {
          res.redirect("/bicicletas");
          return;
        }
      }
    );
  },
  delete: function (req, res, next) {
    Bicicleta.findByIdAndDelete(req.params.id, (err) => {
      if (err) {
        next(err);
      } else {
        res.redirect("/bicicletas");
      }
    });
  },
};
