const passport = require("../config/passport");

module.exports = {
  login: function (req, res, next) {
    res.render("login/login", { title: "Inicio de sesion" });
  },
  login_post: function (req, res, next) {
    passport.authenticate("local", function (err, usuario, info) {
      if (err) return next(err);
      if (!usuario) return res.render("login/login", { info });
      req.flash("usuario", usuario);
      req.logIn(usuario, function (err) {
        if (err) return next(err);
        return res.redirect("/");
      });
    })(req, res, next);
  },
};
