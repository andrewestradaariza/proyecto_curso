const Usuario = require("../models/usuario");

module.exports = {
  list: function (req, res, next) {
    Usuario.find({}, (err, usuarios) => {
      res.render("usuarios/index", {
        usuarios: usuarios,
        title: "Usuarios | Listado",
      });
    });
  },
  create_get: function (req, res, next) {
    res.render("usuarios/create", {
      errors: {},
      usuario: new Usuario(),
      title: "Usuarios | Crear",
    });
  },
  create_post: function (req, res, next) {
    if (req.body.password != req.body.confirm_password) {
      res.render("usuarios/create", {
        errors: {
          confirm_password: { message: "No coinciden las contraseñas" },
        },
        title: "Usuarios | Crear",
        usuario: new Usuario({
          nombre: req.body.nombre,
          email: req.body.email,
        }),
      });
      return;
    }

    Usuario.create(
      {
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password,
      },
      (err, nuevoUsuario) => {
        if (err) {
          console.error(err.message);
          res.render("usuarios/create", {
            errors: err.errors,
            usuario: new Usuario({
              nombre: req.body.nombre,
              email: req.body.email,
            }),
          });
        } else {
          nuevoUsuario.enviar_email_bienvenida();
          res.redirect("/usuarios");
        }
      }
    );
  },
  update_get: function (req, res, next) {
    Usuario.findById(req.params.id, function (err, usuario) {
      res.render("usuarios/update", {
        errors: {},
        usuario: usuario,
        title: "Usuarios | Modificar",
      });
    });
  },
  update_post: function (req, res, next) {
    var update_values = { nombre: req.body.nombre };
    Usuario.findByIdAndUpdate(req.params.id, update_values, function (
      err,
      usuario
    ) {
      if (err) {
        console.error(err);
        res.render("usuarios/update", {
          errors: err.errors,
          usuario: new Usuario({
            nombre: req.body.nombre,
            email: req.body.email,
          }),
        });
      } else {
        res.redirect("/usuarios");
        return;
      }
    });
  },
  delete: function (req, res, next) {
    console.log(req.params.id);
    Usuario.findByIdAndDelete(req.params.id, function (err) {
      if (err) {
        next(err);
      } else {
        res.redirect("/usuarios");
      }
    });
  },
};
