/**Se instancia el modelo */
const Bicicleta = require("../../models/bicicleta");

/**Endpoint para mostrar el listado de las bicicletas */

module.exports = {
  bicicleta_list: function (req, res) {
    Bicicleta.find({}, (err, bicis) => {
      res.status(200).json({ bicicletas: bicis });
    });
  },
  bicicleta_create: function (req, res) {
    var bici = Bicicleta.createInstance(
      req.body.code,
      req.body.color,
      req.body.modelo
    );
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.create(bici, (err, nBici) => {
      if (err) {
        res.status(404).json({ errors: err.message });
      } else {
        res.status(200).json({ bicicleta: bici });
      }
    });
  },
  bicicleta_delete: function (req, res) {
    Bicicleta.findOneAndDelete({ code: req.body.code }, (err, bici) => {
      res.status(204).send();
    });
  },
  bicicleta_update: function (req, res) {
    let update_values = {
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.lat, req.body.lng],
    };
    Bicicleta.findOneAndUpdate(
      { code: req.body.code },
      update_values,
      (err, bici) => {
        if (err) {
          res.status(404).json({ errors: err.message });
        } else {
          res.status(200).json({ bicicleta: update_values });
        }
      }
    );
  },
};

/**
 * endpoint para actualizar una bicicleta sin persistencia
 */
/*
exports.bicicleta_update = function (req, res) {
  if (req.body.id != null) {
    let bici = Bicicleta.findById(req.body.id);
    if (bici.id != null) {
      if (req.body.color != null) bici.color = req.body.color;

      if (req.body.modelo != null) bici.modelo = req.body.modelo;

      if (req.body.lat != null || req.body.lng != null)
        bici.ubicacion = [req.body.lat, req.body.lng];
      res.status(200).json({ bicicleta: bici });
    } else {
      res.status(404);
    }
  } else {
    res.status(501);
  }
};
*/
