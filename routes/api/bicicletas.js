const express = require("express");
const router = express.Router();
var bicicletaApiController = require("../../controllers/api/bicicletasAPIController");

router.get("/", bicicletaApiController.bicicleta_list);
router.post("/create", bicicletaApiController.bicicleta_create);
router.put("/update", bicicletaApiController.bicicleta_update);
router.delete("/delete", bicicletaApiController.bicicleta_delete);

module.exports = router;
