const { Router } = require("express");
var express = require("express");
var router = express.Router();
var passport = require("../config/passport");
const passwordController = require("../controllers/forgotPasswordController");

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated()) return next();
  res.redirect("/login");
}

function checkAuthentication(req, res, next) {
  if (req.isAuthenticated()) {
    //req.isAuthenticated() will return true if user is logged in
    next();
  } else {
    res.redirect("/login");
  }
}

/* GET home page. */
router.get("/", function (req, res, next) {
  console.log("req.flash.usuario: " + JSON.stringify(req.flash("usuario")));
  res.render("index", {
    title: "Bicicletas",
    usuario: req.flash("usuario"),
  });
});

router.get("/", isAuthenticated, function (req, res, next) {
  console.log("req.usuario: " + req.usuario);
  res.render("index", { usuario: req.usuario });
});

router.get("/logout", isAuthenticated, (req, res, next) => {
  req.logout();
  res.redirect("/");
});

router.get(
  "/auth/facebook",
  passport.authenticate("facebook", {
    authType: "rerequest",
    scope: ["public_profile", "email"],
  })
);

router.get(
  "/auth/facebook/callback",
  passport.authenticate("facebook", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  })
);

router.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: ["profile", "email"],
  })
);

router.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/login",
  })
);

router.get("/forgotPassword", passwordController.forgot_password_get);
router.post("/forgotPassword", passwordController.forgot_password_post);
router.get("/resetPassword/:token", passwordController.reset_password_get);
router.post("/resetpassword", passwordController.reset_password_post);

router.get("/politicasdeprivacidad", function (req, res, next) {
  res.render("politicas", {
    title: "Politicas de privacidad",
  });
});

module.exports = router;
