const mongoose = require("mongoose");
const Reserva = require("./reserva");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const saltRounds = 10;
const Token = require("./token");
const mailer = require("../mailer/mailer");

const Schema = mongoose.Schema;

const validateEmail = (email) => {
  const re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z]{2,3})+$/;
  return re.test(email);
};

const UsuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: [true, "El email es obligatorio"],
    validate: [validateEmail, "Ingrese un email valido"],
    match: [/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z]{2,3})+$/],
  },
  password: {
    type: String,
    required: [true, "El password es obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
  provider: String,
  provider_id: { type: String, unique: true },
  photo: String,
});

UsuarioSchema.statics.createInstance = function (nombre, email, password) {
  return new this({
    nombre: nombre,
    email: email,
    password: password,
  });
};

UsuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

UsuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

UsuarioSchema.statics.allUsers = function (cb) {
  return this.find({}, cb);
};

UsuarioSchema.statics.findByNombre = function (aNombre, cb) {
  return this.find({ nombre: new RegExp(aNombre, "i") }, cb);
};

UsuarioSchema.statics.removeByEmail = function (aEmail, cb) {
  return this.deleteOne({ email: aEmail }, cb);
};

UsuarioSchema.statics.updateByEmail = function (
  aEmail,
  aNombre,
  aPassword,
  cb
) {
  const uc = this.findByEmail(aEmail);
  return uc.updateOne({ nombre: aNombre, password: aPassword }, cb);
};

UsuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreateByGoogle(
  condition,
  callback
) {
  const self = this;
  console.log("condition: " + JSON.stringify(condition));
  self.findOne({
    $or: [{ provider_id: condition.id }, { email: condition.emails[0].value }],
  }),
    (err, result) => {
      if (result) {
        callback(err, result);
        console.log("entro en el result: ");
      } else {
        console.log("-------------CONDITION--------------");

        let values = {};
        values.provider = condition.provider;
        values.provider_id = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;

        console.log("--------------VALUES---------------");

        console.log(JSON.stringify(values));
        self.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          return callback(err, result);
        });
      }
    };
};

UsuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
  let reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde: desde,
    hasta: hasta,
  });
  console.log(reserva);
  reserva.save(cb);
};

UsuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _usuarioId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return cb(err);
    }
    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destination,
      subject: "Reseteo de password de cuenta",
      text:
        "Hola,\n\n" +
        "Por favor, para resetear el password de su cuenta haga click en este link: \n" +
        process.env.HOST +
        "/resetPassword/" +
        token.token +
        ".\n",
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return cb(err);
      }
      console.log("Se envio un email para resetear el password");
    });
  });
};

UsuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({
    _usuarioId: this.id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;

  token.save(function (err) {
    if (err) {
      return console.error(err.message);
    }
    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: email_destination,
      subject: "Verificación de cuenta - Red Bicicletas",
      text:
        "Hola, \n\n" +
        "Por favor, para verificar su cuenta haga click en este enlace:\n" +
        process.env.HOST +
        "/token/confirmation/" +
        token.token +
        "\n",
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.error(err.message);
      }
      console.log(
        "Un correo de verificación fue enviado a: " + email_destination
      );
    });
  });
};

// Compile model from schema
module.exports = mongoose.model("Usuario", UsuarioSchema);
