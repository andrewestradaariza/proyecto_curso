var mongoose = require("mongoose");
const Schema = mongoose.Schema;

const tokenSchema = new Schema({
  _usuarioId: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
    ref: "Usuario",
  },
  token: {
    type: String,
    default: false,
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now,
    expires: 43200,
  },
});

module.exports = mongoose.model("Token", tokenSchema);
