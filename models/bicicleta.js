const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: { type: "2dsphere", sparse: true },
  },
});

BicicletaSchema.statics.createInstance = function (
  code,
  color,
  modelo,
  ubicacion
) {
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion,
  });
};

/**Se pueden agregar metodos al modelo */
BicicletaSchema.methods.toString = function () {
  return "code: " + this.code + " | color: " + this.color;
};

BicicletaSchema.statics.allBicis = function (cb) {
  return this.find({}, cb);
};

BicicletaSchema.statics.add = function (aBici, cb) {
  this.create(aBici, cb);
};

BicicletaSchema.statics.findByCode = function (aCode, cb) {
  return this.findOne({ code: aCode }, cb);
};

BicicletaSchema.statics.removeByCode = function (aCode, cb) {
  return this.deleteOne({ code: aCode }, cb);
};

module.exports = mongoose.model("Bicicleta", BicicletaSchema);
/**Agregamos 2 ejemplos 
var a = new Bicicleta(1, "Rojo", "Urbana", [10.794131, -74.917513]);
var b = new Bicicleta(2, "Azul", "Urbana", [10.796254, -74.914412]);*/
