const mongoose = require("mongoose");
const moment = require("moment");

var Schema = mongoose.Schema;

var reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta: { type: mongoose.SchemaTypes.ObjectId, ref: "Bicicleta" },
  usuario: { type: mongoose.SchemaTypes.ObjectId, ref: "Usuario" },
});

reservaSchema.methods.diasDeReserva = function () {
  return moment(this.hasta).diff(moment(this.desde), "days") + 1;
};

module.exports = mongoose.model("Reserva", reservaSchema);
